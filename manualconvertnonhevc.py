#!python3
from pymediainfo import MediaInfo
import os, sys
import fnmatch
import codecs
import locale
# sys.setdefaultencoding() does not exist, here!
#reload(sys)  # Reload does the trick!
#sys.setdefaultencoding('UTF8')
#os.environ["PYTHONIOENCODING"] = "utf-8"
lib = os.path.abspath("C:\Program Files\MediaInfo\MediaInfo.dll")

def getMedInfo(media_file):
    med = os.path.abspath(media_file)
    media_info = MediaInfo.parse(med,library_file=lib)
    for track in media_info.tracks:
        if track.track_type == 'Video':
            #print media_info.tracks[0].file_name
            return media_info
def isHEVC(media_file):
    med = os.path.abspath(media_file)
    media_info = MediaInfo.parse(med,library_file=lib)
    for track in media_info.tracks:
        if track.track_type == 'Video':
            if 'HEVC' in track.codec:
                #print track.codec
                return True
            elif 'HEVC' not in track.codec:
                return False
def writeOut(names, fname):
    outfile = "G:\mkv\\" + fname + ".txt"
    with open(os.path.abspath(outfile),'wb') as f: 
        for n in names:
            f.write(n.encode('utf-8'))
            f.write(("\n").encode('utf-8'))
def oswalk(target):
    #print("checkpoint 1")
    med_list = []
    #fname_list = []
    tobetrans = []
    for r,d,f in os.walk(os.path.abspath(target)):
        for item in fnmatch.filter(f,"*.m??"):
            #print "file: " + item
            #print "r: " + str(r)
            #print "d: " + str(d)
            n = os.path.abspath(os.path.join(r,item))
            #print "Full file path: " + n
            m = getMedInfo(n)
            if isHEVC(n):
                #print "HEVC"
                if m is not None:
                    med_list.append(str(m.tracks[0].complete_name))
                    #print "File: " + m.tracks[0].file_name + "Codec: " + m.tracks[1].codec
            elif not isHEVC(n):
                #print "Not HEVC"
                if m is not None:
                    tobetrans.append(str(m.tracks[0].complete_name))
                    #print "Not HEVC: " + m.tracks[0].file_name + "Codec: " + m.tracks[1].codec
    
    #print str(med_list)
    #print str(tobetrans)
##    print("The following do not need to be transcoded, they are HEVC")
##    for f in med_list:
##        print(str(f))
##    for x in range(0,3):
##        print("----------------------------------")
##    print("\n")
##    #for x in range(0,2):
##    #    print " \n"
##    print("Need to be transcoded to HEVC")
##    
##    for h in tobetrans:
##      print(str(h))
##    m = ""
##    ml = []
##    for n in med_list:
##        m += n + "\n"
##        ml.append(n)
##    print m
##    print str(ml)
##    med_list = []
##    for i in m:
##        med_list.append(i)
##    print med_list
##    h = ""
##    for f in tobetrans:
##        h += f
##    tobetrans = []
##    for i in h:
##        tobetrans.append(i)
##    print med_list
##    print tobetrans
    mli = len(med_list)
    tbti = len(tobetrans)
    #print(mli)
    writeOut(med_list,"m4v_HEVC_files_list")
    #print("m4v_HEVC_files_list.txt has %d entries" % mli)
    writeOut(tobetrans,"need_to_be_transcoded_list")
    #print("need_to_be_transcoded_list.txt has %d entries" % tbti)
    for n in tobetrans:
        if not isHEVC(os.path.abspath(n)):
            f = str('"' + n + '"')
            print(f)
            os.system("G:\mkv\manual.py -a -i %s"% f)
def main():
	oswalk(os.path.abspath(str(sys.argv[1])))

if __name__ == '__main__':
    main()